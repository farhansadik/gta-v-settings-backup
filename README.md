# GTA V Settings Backup


> ## **Graphics Settings** <br>
> *This graphic settings has been created based on **GTX 1050 Ti 4GB**. To get  60 FPS in 1080P gaming.*

| Item Name  | Settings           |
|------------------|---------------------|
| FXAA | ON |
| MSAA | OFF |
| VSync | ON |
| Population Density | FULL|
| Population Verity | FULL |
| Distance Scaling |FULL |
| Texture Quality | Very High |
| Shader Quality| Very High |
| Shadow Quality| Very High |
| Reflection Quality | Very High |
| Reflection MSAA |  OFF|
| Particles Quality| High |
| Water Quality| Very High |
| Grass Quality| Very High |
| Soft Shadows | Softest |
| Post FX| High|
| Motion Blur| NULL|
| Antiseptic Filtering | x16|
| Ambient Ocuxxxxx| High|
| Tasselection | Very High|





Farhan Sadik